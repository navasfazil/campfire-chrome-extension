if (document.URL.match(/(gif|png|jpg|jpeg)$/i) && (navigator.appVersion.indexOf('Chrome/') != -1 || navigator.appVersion.indexOf('Safari/') != -1)) {
	alert('For direct jpg/gif/png url, please fetch image at Add > Pin > From Web');
}

(function(){
	var v = '1.7';

	var pincsiteurl = "http://localhost/aTeamIndia/";
	if (window.jQuery === undefined || window.jQuery.fn.jquery < v) {
		var done = false;
		var script = document.createElement('script');
		script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js';
		script.onload = script.onreadystatechange = function(){
			if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
				done = true;
				pincit();
			}
		};
		document.getElementsByTagName('head')[0].appendChild(script);
	} else {
		pincit();
	}

	var scraper = scraper || {};

	function pincit() {
		(function($) {
			scraper = {
				parser : 'parserDefault',
				videoflag: 0,
				imgarr : [],

				init: function() {
					this.videoflag = 0;
					this.imgarr = [];
					this.parser = 'parserDefault';

					if (document.URL.indexOf('.google.') != -1) {
						this.parser = 'parserGoogle';
					}

					return this;
				},

				getParameterByName: function(name, url) {
					if (!url) url = window.location.href;
					name = name.replace(/[\[\]]/g, "\\$&");
					var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
					results = regex.exec(url);
					if (!results) return null;
					if (!results[2]) return '';
					return decodeURIComponent(results[2].replace(/\+/g, " "));
				},

				fetch: function() {
					this[this.parser]();
					// console.log(this.imgarr);
					display_thumbnails(this.imgarr,this.videoflag);
				},

				parserOg: function(video_id, title) {
					var
						obj = this,
						$og_image = $('head').find('meta[property="og:image"]');

					if ($og_image.length != 0) {
						$('#pincph').attr('src', $og_image.attr('content'));

						var data = [
							$og_image.attr('content'),
							$('#pincph').width(),
							$('#pincph').height()
						];

						if (typeof title != 'undeinfed') {
							data.push(title);
						} else {
							data.push($('title').text());
						}

						if (typeof video_id != 'undeinfed') {
							data.push(video_id);
						}

						obj.imgarr.unshift(data);
					}
				},

				/**
				 *
				 */
				parserDefault: function() {
					var obj = this;

					$('div:not(#pincframe)').find('img').each(function() {
						var $image = $(this);

						$('#pincph').attr('src', $image.attr('src'));

						var data = [
							$image.attr('src'),
							$('#pincph').width(),
							$('#pincph').height()
						];

						if ($image.attr('alt')) {
							data.push($image.attr('alt'));
						}

						obj.imgarr.unshift(data);
					});
				},

				parserGoogle: function() {
					var obj = this;
					// for images
					$('div.rg_meta').each(function() {
						image_info = JSON.parse($(this).text());

						var data = [
							image_info.ou,
							image_info.ow,
							image_info.oh,
							image_info.pt
						];

						obj.imgarr.unshift(data);
						console.log(image_info);
					});

				},

				/**
				 *
				 */

			};
		}(jQuery));

		(window.pincit = function() {
			if (jQuery('#pincframe').length == 0) {
				jQuery('body').css('overflow', 'hidden')
				.append("\
				<div id='pincframe'>\
					<div id='pincframebg'><p>Loading...</p></div>\
					<div id='pincheader'><p id='pincclose'>X</p><p class='submit_images'>Send Now</p><p id='pinclogo'>" + 'Campfire' + "</p></div>\
					<div id='pincVideos'></div>\
					<div id='pincimages'></div>\
					<div id='pinchidden'><img id='pincph'/></div>\
					<style type='text/css'>\
						#pincframe {color: #333;}\
						#pincframebg {background-image: linear-gradient(to top, #e6e6e6 0%, #f7f7f7 100%); display: none; position: fixed; top: 0; right: 0; bottom: 0; left: 0; z-index: 2147483646;}\
						#pincframebg p {background: #999; border-radius: 45px; color: white; font: normal normal bold 16px\/22px Helvetica, Arial, sans-serif; margin: -2em auto 0 -9.5em; padding: 12px; position: absolute; top: 50%; left: 50%; text-align: center; width: 15em;}\
						#pincframe #pincheader {background: white; color: white; height: 50px; margin: 0; overflow: hidden; padding: 0; position: fixed; top: 0; left: 0; text-align: center; width: 100%; z-index: 2147483647;}\
						#pincframe #pincheader #pinclogo {color: black; font: normal normal bold 20px\/20px Helvetica, Arial, sans-serif; margin: 0; padding: 12px 15px 13px 20px;}\
						#pincframe #pincheader #pincclose {background: #e60023; color: white; cursor: pointer; float: right; font: normal normal bold 16px\/16px Helvetica, Arial, sans-serif; line-height: 50px; margin: 0; padding: 0 20px;}\
						.submit_images {background: #5f5857;color: white;cursor: pointer;float: right;font: normal normal bold 16px/16px Helvetica, Arial, sans-serif;line-height: 50px;margin: 0;padding: 0 20px;}\
						.submit_images:hover,.submit_images:focus {background: #e64a19;}\
						#pincimages {position: fixed; top: 60px; left: 0; width: 100%; height: 94%; overflow-x: auto; overflow-y: scroll; text-align: center; z-index: 2147483647;}\
						#pincimages .pincimgwrapper {background: #fcfcfc; cursor: pointer; display: inline-block; height: 200px; margin: 15px; overflow: hidden; position: relative; width: 200px;}\
						#pincimages .pincbutton {background: #e60023; box-shadow: 0 0px 0 0 rgba(0, 0, 0, 1), 0 5px 50px 0 rgba(0, 0, 0, 1); border-radius: 50px; color: white; font: normal normal bold 36px/36px Helvetica, Arial, sans-serif; padding: 8px 16px; display: none; margin-left: -24px; margin-top: -36px; position: absolute; top: 50%; left:50%;}\
						#pincimages .pincdimension {background: white; font: normal normal normal 12px/12px Helvetica, Arial, sans-serif; padding: 13px 0; position: absolute; right: 0; bottom: 0; left: 0;}\
						#pincimages img {width: 100%; height: auto;}\
						#pinchidden { visibility: none; }\
					</style>\
				</div>");

				jQuery('#pincframebg').fadeIn(200);

				var imgarr = [];
				var videoflag = '0';
				var documentURL = document.URL;

				if (documentURL.indexOf("youtube.com/watch") != -1 || documentURL.indexOf("vimeo.com") != -1) {
					-1==documentURL.indexOf("youtube.com/watch")||$('[id*="oneframeb"]').length?documentURL.match(/vimeo.com\/(\d+)($|\/)/)&&!$('[id*="oneframeb"]').length?(video_id=documentURL.split("/")[3],jQuery.getJSON("https://vimeo.com/api/oembed.json?url=" + documentURL,{format:"json"},function(a){imgsrc=a.thumbnail_url,imgarr.unshift([imgsrc,0,0]),videoflag="1",display_thumbnails(imgarr,videoflag)})):-1==documentURL.indexOf("xvideos.com/video")||$('[id*="oneframeb"]').length?documentURL.match(/redtube.com\/(\d+)($|\/)/)&&!$('[id*="oneframeb"]').length?(imgsrc=jQuery('meta[property="og:image"]').attr("content").replace("m.jpg","i.jpg"),imgarr.unshift([imgsrc,582,388]),videoflag="1",display_thumbnails(imgarr,videoflag)):-1==documentURL.indexOf("hardsextube.com/video/")||$('[id*="oneframeb"]').length?-1==documentURL.indexOf("youporn.com/watch/")||$('[id*="oneframeb"]').length?(jQuery("img").each(function(){var a=jQuery(this).prop("src"),b=this.naturalWidth;b||(b=jQuery(this).width());var c=this.naturalHeight;c||(c=jQuery(this).height()),a&&b>=125&&c>=125&&imgarr.unshift([a,b,c])}),jQuery("body, div, span").each(function(){var a=jQuery(this).css("background-image");if("none"!=a){regex=/(?:\(['|"]?)(.*?)(?:['|"]?\))/,imgsrc=regex.exec(a)[1];var b=this.naturalWidth;b||(b=jQuery(this).width());var c=this.naturalHeight;c||(c=jQuery(this).height()),imgsrc&&b>=250&&c>=250&&imgarr.unshift([imgsrc,b,c])}}),display_thumbnails(imgarr,videoflag)):(imgsrc=jQuery("#galleria img:eq(7)").attr("src"),imgarr.unshift([imgsrc,0,0]),videoflag="1",display_thumbnails(imgarr,videoflag)):(imgsrc=jQuery('link[rel="image_src"]').attr("href"),imgarr.unshift([imgsrc,1920,1080]),videoflag="1",display_thumbnails(imgarr,videoflag)):(imgsrc=jQuery("#tabVote > img").attr("src"),$('#pincph').attr('src', imgsrc),imgarr.unshift([imgsrc,0,0]),videoflag="1",display_thumbnails(imgarr,videoflag)):(video_id=document.URL.match("[\\?&]v=([^&#]*)"),imgsrc="https://img.youtube.com/vi/"+video_id[1]+"/0.jpg",imgarr.unshift([imgsrc,0,0]),videoflag="1",display_thumbnails(imgarr,videoflag),jQuery("#movie_player").css("visibility","hidden"));
				} else if(document.URL.indexOf('buck') != -1) {
					
					buck();
				} else if(document.URL.indexOf('prodigalpictures') != -1){
					prodigalpictures();
				} else if(document.URL.indexOf('artofthetitle') != -1) {
					artofthetitle();
				} else if(document.URL.indexOf('behance') != -1 ){
					behance();
				}
				else {
					scraper
						.init()
						.fetch();
				}
			}

			jQuery('#pincheader').on('click', '#pincclose', function() {
				if (documentURL.indexOf('youtube.com/watch') != -1) {
					jQuery('#movie_player').css('visibility','visible');
				}
				jQuery('body').css('overflow', 'visible');
				jQuery('#pincframe').fadeOut(200, function() {
					jQuery(this).remove();
				});
			});



    jQuery(document).ready(function ($) {

    	$('.trigger_img').click(function () {
    // console.log($(this).siblings(".uploads").val());
    // .toggleClass('checked');

         var $$ = $(this).siblings(".uploads")
        if( !$$.is('.checked')){
            $$.addClass('checked');
            $(this).siblings(".uploads").prop('checked', true);
        } else {
            $$.removeClass('checked');
            $(this).siblings(".uploads").prop('checked', false);
        }


});
             $('.submit_images').click(function () {
           



















                 var result = $('input[type="checkbox"]:checked') // this return collection of items checked
                 if (result.length > 0) {
                     var resultString = ""
                     var cc =1;
                     result.each(function () {
                         resultString+= "&imgsrc="+$(this).val();
                         cc++;
                     });
					// alert(resultString);



    /* Send the data using post */
    var url = pincsiteurl+'/wp-content/uploads/save_json.php';
                var posting = $.post(url, {
                    s: resultString
                });

                /* Put the results in a div */
                posting.done(function(data) {

 				var w = 600;
				var h = 760;
				var left = (screen.width/2)-(w/2);
  				var top = (screen.height/2)-(h/2);
				window.open(pincsiteurl+'/itm-settings', 'pincwindow', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
				if (documentURL.indexOf('youtube.com/watch') != -1) {
					jQuery('#movie_player').css('visibility','visible');
				}
				jQuery('body').css('overflow', 'visible');
				jQuery('#pincframe').remove();


                });














                 }

             });
         });





			// jQuery('#pincimages').on('click', '.pincimgwrapper', function() {
			// 	alert('click');
			// 	// console.log(jQuery(this).data('href'));
			// 	var w = 600;
			// 	var h = 760;
			// 	var left = (screen.width/2)-(w/2);
  	// 			var top = (screen.height/2)-(h/2);
			// 	window.open(jQuery(this).data('href'), 'pincwindow', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
			// 	if (documentURL.indexOf('youtube.com/watch') != -1) {
			// 		jQuery('#movie_player').css('visibility','visible');
			// 	}
			// 	jQuery('body').css('overflow', 'visible');
			// 	jQuery('#pincframe').remove();
			// });

			// jQuery('#pincimages').on('mouseover', '.pincimgwrapper', function() {
			// 	jQuery(this).find('.pincbutton').show();
			// }).on('mouseout', '.pincimgwrapper', function() {
			// 	jQuery(this).find('.pincbutton').hide();
			// });

			jQuery(document).keyup(function(e) {
				if (e.keyCode == 27) {
				if (documentURL.indexOf('youtube.com/watch') != -1) {
					jQuery('#movie_player').css('visibility','visible');
				}
				jQuery('body').css('overflow', 'visible');
				jQuery('#pincframe').fadeOut(200, function() {
					jQuery(this).remove();
				});
				}
			});
		})();
	}

	function display_thumbnails(imgarr, videoflag) {
		console.log(imgarr);
		if (!imgarr.length) {
			jQuery('#pincframebg').html('<p>Sorry, unable to find anything to save on this page.</p>');
		} else if (document.URL.match(/(gif|png|jpg|jpeg)$/i)) {
			jQuery('#pincimages').hide();
			jQuery('#pincframebg').html('<p>For direct jpg/gif/png url,<br />please fetch image at<br /><a href="' + pincsiteurl + '/itm-settings/">Add > Pin > From Web</a></p>');
		} else {
			imgarr.sort(function(a,b)
			{
				if (a[1] == b[1]) return 0;
				return a[1] > b[1] ? -1 : 1;
			});

			var imgstr = '';
			for (var i = 0; i < imgarr.length; i++) {
				if (typeof imgarr[i][0] == 'undefined') {
					continue;
				}

				page_title = document.getElementsByTagName('title')[0].innerHTML;

				if (typeof imgarr[i][3] != 'undefined' && imgarr[i][3].length != 0) {
					page_title = imgarr[i][3];
				}

				video_id = '';

				if (typeof imgarr[i][4] != 'undefined') {
					video_id = imgarr[i][4];
				}

				if(document.URL.indexOf('buck') != -1)
				{
					var source = "https://vimeo.com/"+imgarr[i][4];
				}else if(document.URL.indexOf('prodigalpictures') != -1) {
					var source = "https://vimeo.com/"+imgarr[i][4];
				}else if(document.URL.indexOf('artofthetitle') != -1){
					var source = "https://vimeo.com/"+imgarr[i][4];
				}else if(document.URL.indexOf('behance') != -1){
					var source = "https://vimeo.com/"+imgarr[i][4];
				}else {
					var source = document.URL;
				}

				if (videoflag == '0' || imgarr[i][1] != 0 ) {
					imgstr += '<div class="pincimgwrapper" data-href="'
					+ pincsiteurl + 'itm-settings/?m=bm&imgsrc='
					+ encodeURIComponent(imgarr[i][0].replace('http', ''))
					+ '&source=' + encodeURIComponent(document.URL.replace('http', ''))
					+ '&title=' + encodeURIComponent(page_title)
					+ '&video=' + videoflag + '&video_id=' + video_id
					+ '">'
					// + '<div class="pincbutton">+</div>'
					+ '<div class="pincdimension">'
					+ parseInt(imgarr[i][1],10) + ' x '
					+ parseInt(imgarr[i][2],10) + '</div><input type="checkbox" name="uploads[]" class="uploads" value="'+imgarr[i][0]+'"><img class="trigger_img" src="' + imgarr[i][0]
					+ '" /></div>';
				} else if((videoflag == '1' || imgarr[i][1] == 0 )) {
					imgstr += '<div class="pincimgwrapper" data-href="' + pincsiteurl
					+ 'itm-settings/?m=bm&imgsrc='
					+ encodeURIComponent(imgarr[i][0].replace('http', ''))
					+ '&source=' + encodeURIComponent(source.replace('http', ''))
					+ '&title=' + encodeURIComponent(page_title)
					+ '&video=' + videoflag + '&video_id=' + video_id
					+ '"><div class="pincbutton">+</div><div class="pincdimension"> Video </div><img src="'
					+ imgarr[i][0] + '" /></div>';
				}
				// console.log(imgarr[i][0]);
				// console.log(page_title);
			}

			jQuery('#pincframebg p').fadeOut(200);
			jQuery('#pincimages').css('height',jQuery(window).height()-jQuery('#pincheader').height()-20)
								.html(imgstr + '<div style="height:40px;clear:both;"><br /></div>');
			jQuery('#pincVideos').css('height',jQuery(window).height()-jQuery('#pincheader').height()-20)
			.html('navasfazil');
			if ((navigator.appVersion.indexOf('Chrome/') != -1 || navigator.appVersion.indexOf('Safari/')) && videoflag != '1') {
				jQuery('#pincimages .pincimgwrapper').css('float','left');
			}
		}
	}

function buck()
{
	var imgarr=[];var obj={};var checked=true;$('.player').each(function(num){let width=0;let height=0;var data=[];var abc=document.querySelectorAll('.slide .player')[num].attributes.src.nodeValue.split('?')[0].split('/').slice(-1).pop();$.getJSON("https://vimeo.com/api/oembed.json?url=https://vimeo.com/"+abc,function(result){$.each(result,function(i,field){if(i=="thumbnail_url")source=field;if(i=="title")alt_tag=field});data=[source,width,height,alt_tag,abc];if(checked){checked=false;$('div:not(#pincframe)').find('img').each(function(){var $image=$(this);$('#pincph').attr('src',$image.attr('src'));var data=[$image.attr('src'),$('#pincph').width(),$('#pincph').height()];if($image.attr('alt')){data.push($image.attr('alt'))}imgarr.unshift(data)})}imgarr.unshift(data);display_thumbnails(imgarr,"1")})});		
}
function prodigalpictures()
{
	var imgarr=[];var obj={};var checked=true;$('.fancybox-media').each(function(num){if(document.querySelectorAll('.fancybox-media')[num].href.indexOf('vimeo')!=-1)var id=document.querySelectorAll('.fancybox-media')[num].href.split('/').slice(-1).pop();let width=0;let height=0;var data=[];$.getJSON("https://vimeo.com/api/oembed.json?url=https://vimeo.com/"+id,function(result){$.each(result,function(i,field){if(i=="thumbnail_url")source=field;if(i=="title")alt_tag=field});data=[source,width,height,alt_tag,id];if(checked){checked=false;$('div:not(#pincframe)').find('img').each(function(){var $image=$(this);$('#pincph').attr('src',$image.attr('src'));var data=[$image.attr('src'),$('#pincph').width(),$('#pincph').height()];if($image.attr('alt')){data.push($image.attr('alt'))}imgarr.unshift(data)})}imgarr.unshift(data);display_thumbnails(imgarr,"1")})});
}
function artofthetitle()
{
	var imgarr=[];var obj={};var checked=true;$('.video-loaded').each(function(num){var id=document.querySelectorAll('.video-loaded')[num].getAttribute("data-vimeoid");let width=0;let height=0;var data=[];$.getJSON("https://vimeo.com/api/oembed.json?url=https://vimeo.com/"+id,function(result){$.each(result,function(i,field){if(i=="thumbnail_url")source=field;if(i=="title")alt_tag=field});data=[source,width,height,alt_tag,id];if(checked){checked=false;$('div:not(#pincframe)').find('img').each(function(){var $image=$(this);$('#pincph').attr('src',$image.attr('src'));var data=[$image.attr('src'),$('#pincph').width(),$('#pincph').height()];if($image.attr('alt')){data.push($image.attr('alt'))}imgarr.unshift(data)})}imgarr.unshift(data);display_thumbnails(imgarr,"1")})})
}
function behance(){
	var imgarr=[];var obj={};var checked=true;$('.embed-aspect-ratio > iframe').each(function(num){var id=$('.embed-aspect-ratio > iframe')[num].src.split('/').slice(-1).pop();let width=0;let height=0;var data=[];$.getJSON("https://vimeo.com/api/oembed.json?url=https://vimeo.com/"+id,function(result){$.each(result,function(i,field){if(i=="thumbnail_url")source=field;if(i=="title")alt_tag=field});data=[source,width,height,alt_tag,id];if(checked){checked=false;$('div:not(#pincframe)').find('img').each(function(){var $image=$(this);$('#pincph').attr('src',$image.attr('src'));var data=[$image.attr('src'),$('#pincph').width(),$('#pincph').height()];if($image.attr('alt')){data.push($image.attr('alt'))}imgarr.unshift(data)})}imgarr.unshift(data);display_thumbnails(imgarr,"1")})});
}

})();



